<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jenis_barang_model");
		
		//cek sesi login
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
		
		//load validasi
		$this->load->library("form_validation");
	}
	
	public function index()
	{
		$this->listjenisbarang();
	}
	
	public function listjenisbarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['content']			=	'form/Jenis_barang/Home_jenis_barang';
		
		$this->load->view('home', $data);
	}
	
	public function input()
	{
		$data['content']			=	'form/Jenis_barang/Input_jenis_barang';
		
		/*if (!empty($_REQUEST)) {
			$m_jenis_barang = $this->jenis_barang_model;
			$m_jenis_barang->save();
			redirect("jenis_barang/index", "refresh");
		}*/
		
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run())
		{
			$this->jenis_barang_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("Jenis_barang/index", "refresh");	
		}
		
		$this->load->view('home', $data);
	}
	
	public function detailjenisbarang($kode_jenis)
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$data['content']			=	'form/Jenis_barang/Detail_jenis_barang';
		
		$this->load->view('home', $data);
	}
	
	public function edit($kode_jenis)
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$data['content']			=	'form/Jenis_barang/Edit_jenis_barang';
		
		/*if (!empty($_REQUEST)) {
			$m_jenis_barang = $this->jenis_barang_model;
			$m_jenis_barang->update($kode_jenis);
			redirect("jenis_barang/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run())
		{
			$this->jenis_barang_model->update($kode_jenis);
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Update Data Berhasil !</div>');
			redirect("Jenis_barang/index", "refresh");	
		}

		$this->load->view('home', $data);
	}
	
	public function delete($kode_jenis)
	{
		$m_jenis_barang = $this->jenis_barang_model;
		$m_jenis_barang->delete($kode_jenis);
		redirect("Jenis_barang/index", "refresh");
	}
	
}