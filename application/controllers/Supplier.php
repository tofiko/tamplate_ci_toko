<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("supplier_model");
		
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
		//load validasi
		$this->load->library("form_validation");
	}
	
	public function index()
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']	=	'form/Supplier/Home_supplier';
		$this->load->view('home', $data);
	}
	
	/*public function input()
	{
		$data['content']	=	'form/Supplier/Input_supplier';
		if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->save();
			redirect("supplier/index", "refresh");
		}
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->supplier_model->rules());
		
		if ($validation->run())
		{
			$this->supplier_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("supplier/index", "refresh");	
		}
		
		$this->load->view('home',$data);
	}*/
	
	public function input()
	{
		$data['content']	=	'form/Supplier/Input_supplier';
		/*if (!empty($_REQUEST)) {
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->save();
			redirect("jabatan/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->supplier_model->rules());
		
		if ($validation->run())
		{
			$this->supplier_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("Supplier/index", "refresh");	
		}
		
		$this->load->view('home',$data);
	}
	
	public function detailsupplier($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$data['content']	=	'form/Supplier/Detail_supplier';
		
		$this->load->view('home' , $data);
	}
	
	public function edit($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
		$data['content']	=	'form/Supplier/Edit_supplier';
		
		/*if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->update($kode_supplier);
			redirect("supplier/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->supplier_model->rules());
		
		if ($validation->run())
		{
			$this->supplier_model->update($kode_supplier);
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Update Data Berhasil !</div>');
			redirect("supplier/index", "refresh");	
		}
		$this->load->view('home', $data);
	}
	
	public function delete($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
	}
	
}