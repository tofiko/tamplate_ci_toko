<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		
		//load validasi
		$this->load->library("form_validation");
		
		//cek sesi login
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
	}
	
	public function index()
	{
		$this->listbarang();
	}
	
	public function listbarang()
	{
		$data['data_jenis_barang'] 	= $this->jenis_barang_model->tampilDataJenisBarang();
		$data['data_barang'] 		= $this->barang_model->tampilDataBarang2();
		$data['content']			=	'form/Barang/Home_barang';
		$this->load->view('home', $data);
	}
	
	public function input()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['content']			=	'form/Barang/Input_barang'; 
		
		/*if (!empty($_REQUEST)) {
			$m_barang = $this->barang_model;
			$m_barang->save();
			redirect("barang/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->barang_model->rules());
		
		if ($validation->run())
		{
			$this->barang_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("Barang/index", "refresh");	
		}
		
		$this->load->view('home', $data);
	}
	
	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']			=	'form/Barang/Detail_barang';
		$this->load->view('home', $data);
	}
	
	public function edit($kode_barang)
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']			=	'form/Barang/Edit_barang';
		
		/*if (!empty($_REQUEST)) 
		{
			$m_barang = $this->barang_model;
			$m_barang->update($kode_barang);
			redirect("barang/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->barang_model->rules());
		
		if ($validation->run())
		{
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Update Data Berhasil !</div>');
			redirect("Barang/index", "refresh");	
		}

		$this->load->view('home', $data);
	}
	
	public function delete($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index", "refresh");
	}
	
}