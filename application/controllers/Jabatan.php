<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("jabatan_model");
		
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
		
		//load validasi
		$this->load->library("form_validation");
	}
	
	public function index()
	{
		$this->listjabatan();
	}
	
	public function listjabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']	=	'form/Jabatan/Home_jabatan';
		
		$this->load->view('home', $data);
	}
	
	public function input()
	{
		$data['content']	=	'form/Jabatan/Input_jabatan';
		/*if (!empty($_REQUEST)) {
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->save();
			redirect("jabatan/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->jabatan_model->rules());
		
		if ($validation->run())
		{
			$this->jabatan_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("Jabatan/index", "refresh");	
		}
		
		$this->load->view('home',$data);
	}
	
	public function detailjabatan($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content']	=	'form/Jabatan/Detail_jabatan';
		
		$this->load->view('home', $data);
	}
	
	public function edit($kode_jabatan)
	{
		$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
		$data['content']	=	'form/Jabatan/Edit_jabatan';
		
		/*if (!empty($_REQUEST)) {
			$m_jabatan = $this->jabatan_model;
			$m_jabatan->update($kode_jabatan);
			redirect("jabatan/index", "refresh");
		}*/
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->jabatan_model->rules());
		
		if ($validation->run())
		{
			$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Update Data Berhasil !</div>');
			redirect("Jabatan/index", "refresh");	
		}
		
		$this->load->view('home', $data);
	}
	
	public function delete($kode_jabatan)
	{
		$m_jabatan = $this->jabatan_model;
		$m_jabatan->delete($kode_jabatan);
		redirect("jabatan/index", "refresh");
	}
	
}