<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("karyawan_model");
		$this->load->model("supplier_model");
		$this->load->model("pembelian_model");
		$this->load->model("barang_model");
		
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
		
		//load validasi
		$this->load->library("form_validation");
	}
	
	public function index()
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('home_karyawan', $data);
	}
	
	public function input_h()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']			=	'form/Pembelian/Input_pembelian';
		
		/*if (!empty($_REQUEST)) {
			$m_pembelian_h = $this->pembelian_model;
			$m_pembelian_h->savePembelianHeader();
			
			//panggil ID transaksi terakhir
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $id_terakhir, "refresh");
		}*/
		
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->pembelian_model->rulesH());
		
		if ($validation->run())
		{
			$m_pembelian_h = $this->pembelian_model;
			$m_pembelian_h->savePembelianHeader();
			
			//panggil ID transaksi terakhir
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $id_terakhir, "refresh");	
		}
		
		$this->load->view('home', $data);
		
		//$this->load->view('input_pembelian', $data);
	}

	/*public function input_d($id)
	{
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id);
		$data['id_header']				= $this->pembelian_model->tampilDataPembelian();
		
		if (!empty($_REQUEST)) {
			$m_pembelian_d = $this->pembelian_model;
			$m_pembelian_d->savePembelianDetail();
			
			//panggil ID transaksi terakhir
			$last_id = $m_pembelian_d->idTransaksiTerakhir();
			
			redirect("pembelian/input_d/" . $last_id, "refresh");
		}
		$this->load->view('input_pembelian_d', $data);
	}*/
	
	public function input_d($id_pembelian_header)
	{
		
		$data['id_header']				= $id_pembelian_header;
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
		$data['content']				= 'form/Pembelian/Input_pembelian_d';
		
		/*if (!empty($_REQUEST)) {
			
			//save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			//exit;
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		}*/
		
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->pembelian_model->rulesD());
		
		if ($validation->run())
		{
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_d/" . $id_terakhir, "refresh");	
		}
		
		$this->load->view('home', $data);
		//$this->load->view('input_pembelian_d', $data);
	}
	
	/*public function input_d($id_pembelian_header)
	{
		
		$data['id_header']				= $id_pembelian_header;
		$data['data_barang'] 			= $this->barang_model->tampilDataBarang();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
		
		if (!empty($_REQUEST)) {
			
			//save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			//exit;
			//proses update stok
			$kode_barang	= $this->input->post('kode_barang');
			$qty			= $this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_d/" . $id_pembelian_header , "refresh");
		}
		$this->load->view('input_pembelian_d', $data);
	}*/
}