<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataKaryawan2()
	{
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function rules()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'nik',
			'label'		=> 'NIK',
			'rules' 	=> 'required|max_length[10]',
			'errors'	=>	[
								'required'		=>	'NIK Tidak Boleh Kosong.',
								'max_length'	=>	'NIK Tidak Boleh Lebih Dari 10 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'nama_lengkap',
			'label'		=> 'Nama Lengkap',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Lengkap Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'tempat_lahir',
			'label'		=> 'Tempat Lahir',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Tempat Lahir Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'telp',
			'label'		=> 'Telp',
			'rules' 	=> 'required|max_length[15]',
			'errors'	=>	[
								'required'		=>	'Telp Tidak Boleh Kosong.',
								'max_length'	=>	'Telp Tidak Boleh Lebih Dari 15 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'alamat',
			'label'		=> 'Alamat',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Alamat Tidak Boleh Kosong.'
							]
			],
		];	
	}
	
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nik']			= $this->input->post('nik');
		$data['nama_lengkap']	= $this->input->post('nama_lengkap');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl_lahir']		= $tgl_gabung;
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['kode_jabatan']	= $this->input->post('kode_jabatan');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
		
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']	= $this->input->post('nama_lengkap');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl_lahir']		= $tgl_gabung;
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['kode_jabatan']	= $this->input->post('kode_jabatan');
		$data['flag']			= 1;
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($nik)
	{
		//delete from db
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}
}
