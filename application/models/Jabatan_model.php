<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_model extends CI_Model
{
	//panggil nama table
	private $_table = "jabatan";
	
	public function tampilDataJabatan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataJabatan2()
	{
		$query = $this->db->query("SELECT * FROM jabatan WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataJabatan3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_jabatan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function rules()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'kode_jabatan',
			'label'		=> 'Kode Jabatan',
			'rules' 	=> 'required|max_length[5]',
			'errors'	=>	[
								'required'		=>	'Kode Jabatan Tidak Boleh Kosong.',
								'max_length'	=>	'Kode Jabatan Tidak Boleh Lebih Dari 5 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'nama_jabatan',
			'label'		=> 'Nama Jabatan',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Jabatan Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'keterangan',
			'label'		=> 'Keterangan',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Keterangan Tidak Boleh Kosong.'
							]
			]
		];	
	}
	
	public function save()
	{
		
		$data['kode_jabatan']	= $this->input->post('kode_jabatan');
		$data['nama_jabatan']	= $this->input->post('nama_jabatan');
		$data['keterangan']		= $this->input->post('keterangan');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_jabatan)
	{
		$data['nama_jabatan']	= $this->input->post('nama_jabatan');
		$data['keterangan']		= $this->input->post('keterangan');
		$data['flag']			= 1;
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_jabatan)
	{
		//delete from db
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->delete($this->_table);
	}

}
