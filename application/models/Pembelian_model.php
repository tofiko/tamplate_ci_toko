<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";
	
	public function tampilDataPembelian()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1" );
		return $query->result();
	
	}
	
	public function rulesH()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'no_transaksi',
			'label'		=> 'Nomor Transaksi',
			'rules' 	=> 'required|max_length[10]',
			'errors'	=>	[
								'required'		=>	'Nomor Transaksi Tidak Boleh Kosong.',
								'max_length'	=>	'Nomor Transaksi Tidak Boleh Lebih Dari 10 Karakter.'
							]
			]
		];	
	}
	
	public function savePembelianHeader()
	{
		$data['no_transaksi']	= $this->input->post('no_transaksi');
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['tanggal']		= date('Y-m-d');
		$data['approved']		= 1;
		$data['flag']			= 1;
		$this->db->insert($this->_table_header, $data);
	}
	
	public function idTransaksiTerakhir()
	{
		$query = $this->db->query(
			"SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY
			id_pembelian_h DESC LIMIT 0,1" );
			$data_id = $query->result();
			
			foreach ($data_id as $data){
				$last_id = $data->id_pembelian_h;
			}
		return $last_id;
	}
	
	/*public function tampilDataPembelianDetail($id)
	{
		$query = $this->db->query(
			"SELECT A.id_pembelian_d,B.nama_barang FROM " . $this->_table_detail . " AS A
			INNER JOIN barang AS B ON A.kode_barang=B.kode_barang
			WHERE A.flag = 1 AND A.id_pembelian_h='$id'");
		return $query->result();
	
	}*/
	
	Public function tampilDataPembelianDetail($id_pembelian_header)
	{			
		$query = $this->db->query(
			"SELECT A. *,  B.nama_barang FROM " . $this->_table_detail . " AS A
			INNER JOIN `barang` AS B ON A.kode_barang = B.kode_barang
			WHERE A.`flag` = '1' AND A.`id_pembelian_h`  = ". $id_pembelian_header 
			);
			$data = $query->result();
			
			
		return $query->result();
	}
	
	public function rulesD()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'qty',
			'label'		=> 'QTY',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'QTY Tidak Boleh Kosong.',
								'numeric'		=>	'QTY harus berisi angka.'
							]
			],
			[
			'field' 	=> 'harga_barang',
			'label'		=> 'Harga Barang',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'Harga Barang Tidak Boleh Kosong.',
								'numeric'		=>	'Harga Barang harus berisi angka.'
							]
			]
		];	
	}
	
	public function savePembelianDetail($id_pembelian_header)
	{
		$harga = $this->input->post('harga');
		$qty = $this->input->post('qty');
		
		$data['id_pembelian_h']	= $id_pembelian_header;
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['qty']			= $qty;
		$data['harga']			= $harga;
		$data['jumlah']			= $harga * $qty;
		$data['flag']			= 1;
		$this->db->insert($this->_table_detail, $data);
	}
}
