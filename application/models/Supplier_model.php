<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends CI_Model
{
	//panggil nama table
	private $_table = "supplier";
	
	public function tampilDataSupplier()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataSupplier2()
	{
		$query = $this->db->query("SELECT * FROM supplier WHERE flag = 1");
		return $query->result();
	
	}
	
	public function tampilDataSupplier3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_supplier', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function rules()
	{
		return
		[
			[
			//form input
			//field dari name input
			'field' 	=> 'kode_supplier',
			'label'		=> 'Kode Supplier',
			'rules' 	=> 'required|max_length[5]',
			'errors'	=>	[
								'required'		=>	'Kode Supplier Tidak Boleh Kosong.',
								'max_length'	=>	'Kode Supplier Tidak Boleh Lebih Dari 5 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'nama_supplier',
			'label'		=> 'Nama Supplier',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Supplier Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'alamat',
			'label'		=> 'Alamat',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Alamat Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'telp',
			'label'		=> 'Telp',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Telp Tidak Boleh Kosong.'
							]
			]
		];	
	}
	
	public function save()
	{
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_supplier)
	{
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['flag']			= 1;
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_supplier)
	{
		//delete from db
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->delete($this->_table);
	}

}
