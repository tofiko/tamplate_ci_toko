<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "barang";
	
	public function tampilDataBarang()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	
	}
	
	public function tampilDataBarang2()
	{
		$query = $this->db->query("SELECT * FROM jenis_barang as jb inner join barang as br on
		jb.kode_jenis=br.kode_jenis");
		return $query->result();
	
	}
	
	public function tampilDataBarang3()
	{
		$this->db->select('*');
		$this->db->order_by('kode_barang', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function rules()
	{
		return
		[
			[
			//form input
			'field' 	=> 'kode_barang',
			'label'		=> 'Kode Barang',
			'rules' 	=> 'required|max_length[5]',
			'errors'	=>	[
								'required'		=>	'Kode Barang Tidak Boleh Kosong.',
								'max_length'	=>	'Kode Barang Tidak Boleh Lebih Dari 5 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'nama_barang',
			'label'		=> 'Nama Barang',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Barang Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'harga_barang',
			'label'		=> 'Harga Barang',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'Harga Barang Tidak Boleh Kosong.',
								'numeric'		=>	'Harga Barang Harus Angka.'
							]
			],
				
			[			
			'field' 	=> 'kode_jenis',
			'label'		=> 'Kode Jenis',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Kode Jenis Tidak Boleh Kosong.'
							]
			],
				
			/*[				
			'field' 	=> 'stok',
			'label'		=> 'Stok',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Stok Tidak Boleh Kosong.'
							]
			]*/
		];	
	}
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_barang)
	{
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_barang)
	{
		//delete from db
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}

}
