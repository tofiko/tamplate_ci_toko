<div class="blog">
	<div class="conteudo">
    	<div class="post-info">
        	<b>EDIT DATA SUPPLIER</b><br>
        </div>
</div>
<?php
	foreach ($detail_supplier as $data) {
		$kode_supplier	= $data->kode_supplier;
		$nama_supplier	= $data->nama_supplier;
		$alamat			= $data->alamat;
		$telp			= $data->telp;
	}
?>
<form action="<?=base_url()?>supplier/edit/<?=$kode_supplier;?>" method="post">
<div style="color: blue" align="center"><?=validation_errors();?></div>
<table width="1350px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#3141ff">
  <tr>
    <td>Kode Supplier</td>
    <td>:</td>
    <td>
      <input type="text" name="kode_supplier" id="kode_supplier" value="<?=$kode_supplier;?>" readonly>
    </td>
  </tr>
  <tr>
    <td>Nama Supplier</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_supplier" id="nama_supplier" value="<?=$nama_supplier;?>" maxlength="50">
    </td>
  </tr>
  <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat;?></textarea></td>
  </tr>
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value="<?=$telp;?>" /></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <input type="submit" name="Submit" id="Submit" value="Simpan">
    <input type="reset" name="reset" id="reset" value="Batal">
    </td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
    <a href="<?=base_url();?>supplier/listsupplier">
    <input type="button" name="Submit" id="Submit" value="Kembali"></a>
    </td>
  </tr>
</table>
</form>
</div>
