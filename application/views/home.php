<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$user_login	= $this->session->userdata();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>Aplikasi Sederhana</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/style.css">
</head>
<body>
	<header class="header">
        	<?php
            	if($user_login['tipe'] == "1")
				{
					$this->load->view('tamplate/view_menu');
				}
				else
				{
					$this->load->view('tamplate/view_menu_user');	
				}
			?>
            </br>
            <!--ini isi form-->
            <?php
            	$this->load->view($content);
			?>
    </header>
</body>
</html>